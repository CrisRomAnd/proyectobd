## Descargar git

Hacen click en este enlace para descargar [GIT para windows](https://git-scm.com/download/win) y lo instalan.

si lo pueden agregar al PATH mejor

Ahora buscan Git bash que abrirá un terminal para git y van ejecutar lo siguiente con sus respectivos datos, no importa el correo.

```bash
git config --global user.name "NOMBRE"
git config --global user.email USUARIO@CORREO.com
```

## Descarga del proyecto

Se abren la cuenta en https://bitbucket.org, mejor si lo hacen con la cuenta de la unam. Me pasan el correo o el usuario con lo que crearon la cuenta.

En **git bash** con el comando cd se ubican a su carpeta Documentos

```bash
cd C:ruta/a/Documentos
```

y clonan el repositorio(__Ya debieron tener cuenta en bitbucket__, la cuenta de la unam otorga el pack de estudiante)

```bash
git clone https://CrisRomAnd@bitbucket.org/CrisRomAnd/proyectobd.git
```

## Visual studio

Para usar git de forma gráfica se puede usar el [code](https://code.visualstudio.com/) o el [atom](https://atom.io/)

cuando este instalado abriran el code, se van a **archivo** - **Abrir carpeta**  y se van all directorio del proyecto.

Cuando hagan una modificacion se van al tercer icono de la barra lateral de la derecha.

Seleccionan el simbolo **+** del archivo que modificaron y ponen una nota corta de lo que sea que modificaron y le ponen palomita.

![Code](img/code.png)

Para subir al repo se van hasta abajo donde dice master a lado aparece una flechita, le dan click aqui, aceptar e ingresan su usuario y contraseña.

![Code](img/code2.png)

Ahi mismo sirve para hacer actualizaciones y bajaar cambio de terceros(Fetch y Pull).